define("tests/Entity", ["module", "Arstider/Entity"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);


	/**
	 * Tests
	 */
	QUnit.test("Entity parameters.", function(assert){
		var params = {
			x: 5,
			y: 15,
			width: 100,
			height: 44,
			rotation: 90,
			scaleX: 1.5,
			scaleY: 1.4,
			rpX: 50,
			rpY: 20,
			alpha: 0.3
		};
		var e = new subject(params);
		
		assert.ok(e, "New entity.");
		
		// Check params
		var success = true;
		for (var key in params){
			if (!(key in e && e[key] === params[key])){
				success = false;
				break;
			}
		}
		assert.ok(success, "Correct entity parameters.");
	});
	
	QUnit.test("Touch detection", function(assert){
		var e = new subject({
			x: 5,
			y: 15,
			width: 100,
			height: 44
		});
		
		assert.ok(e, "New entity.");
		
		e.updateGlobalProperties();
		
		e.touchAccuracy = subject.PASSIVE;
		assert.ok(!e.isTouched(65, 27), "No touch in, passive accuracy");
		
		e.touchAccuracy = subject.ACTIVE;
		assert.ok(e.isTouched(65, 27), "Touch down, active accuracy");
		assert.ok(!e.isTouched(105, 59), "No touch for corner, active accuracy");
		assert.ok(!e.isTouched(0, 0), "No touch outside, active accuracy");
		
		e.touchAccuracy = subject.COMPLEX;
		assert.ok(e.isTouched(65, 27), "Touch down, complex accuracy");
		assert.ok(!e.isTouched(105, 59), "No touch for corner, complex accuracy");
		assert.ok(!e.isTouched(0, 0), "No touch outside, complex accuracy");
	});
	
	QUnit.test("Local collision detection", function(assert){
		var e = new subject({
			x: 5,
			y: 15,
			width: 100,
			height: 50
		});
		
		assert.ok(e, "New entity.");
				
		assert.ok(e.collides(65, 27, 10, 10), "Collision");
		assert.ok(!e.collides(-10, 0, 10, 10), "No collision");
		assert.ok(!e.collides(105, 45, 10, 10), "No collision on corner touch");
	});
});		