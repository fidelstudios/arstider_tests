define("tests/Fonts", ["module", "Arstider/Fonts"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id, {
	    afterEach: function() {
            var loader = window.document.getElementById("Arstider_font_loader");
            if (loader)
            {
                loader.parentNode.removeChild(loader);
            }
	    }
	});

	/**
	 * Tests
	 */
	QUnit.test("Font loading.", function(assert){
	    var done = assert.async();
	    
        subject.load("media/fonts/fonts.json", function(){
            assert.ok(true, "JSON loaded.");
            
            var font = subject.get("btnLargeFont");
            
            assert.ok(font.name == "btnLargeFont", "Font was loaded.");
            
            done();
        });
	});
	
	QUnit.test("Font creation.", function(assert){
	    subject.create({
            "name": "overlayFont",
            "url": "media/fonts/Dosis-Bold.woff",
            "size": "28px",
            "fontOffsetY": 28,
            "fillStyle": "#000",
            "textBaseline": "alphabetical",
            "lineSpacing": 30,
            "textAlign": "center"
        });
        
        var font = subject.get("overlayFont");
            
        assert.ok(font.name == "overlayFont", "Font was loaded.");
	});
	
    QUnit.test("Class singleton integrity.", function(assert){
        var done = assert.async();
        subject.__testVal = "test";
        require(["Arstider/Fonts"], function(s){
            assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
            done();
            //Cleanup
            delete subject.__testVal;
        });
    });
});