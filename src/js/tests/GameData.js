define("tests/GameData", ["module", "Arstider/GameData"], function (module, subject) {    
    
    /**
     * Setup
     */
    QUnit.module(module.id, {
        afterEach: function() {
            // Clean up after test, to test in clean environment
            subject._defaultSet = {};
            subject._runtimeSet = {};
            subject.clearStorage();
        }
    });

    /**
     * Tests
     */
    QUnit.test("GameData loading.", function(assert){
        var done = assert.async();
        
        assert.ok(subject.get("test") === undefined, "Value does not exist prior to loading.");
        
        subject.load("media/configs/GameConfig.json", function(){
            assert.ok(true, "JSON loaded.");
            assert.equal(subject.get("test"), 55, "Value correct.");
            done();
        });
    });
    
    QUnit.test("GameData set.", function(assert){
        // Test value set
        assert.notEqual(subject.get("test"), 55, "Value no yet set.");
        
        subject.set("test", 55);
        assert.equal(subject.get("test"), 55, "Value was set correctly.");
        
        //  Test value overwrite
        subject.set("test", 65);
        assert.equal(subject.get("test"), 65, "Value was overwritten correctly.");
        
        // Test value history
        subject.revert("test", 1);
        assert.equal(subject.get("test"), 55, "Value was reverted correctly.");
        subject.redo("test", 1);
        assert.equal(subject.get("test"), 65, "Value was redone correctly.");
        
        // Test value storage
        subject.setStoragePrefix("testing");
        assert.notEqual(subject.get("test_storage", true), 55, "Value no yet set.");
        assert.notEqual(subject.get("test_storage", true), 55, "Value still not set.");
        
        subject.set("test_storage", 55, true);
        assert.equal(subject.get("test_storage"), 55, "Value was set correctly in gamedata.");
        
        subject.unset("test_storage");
        assert.notEqual(subject.get("test_storage"), 55, "Value was correctly removed from gamedata.");
        assert.equal(subject.get("test_storage", true), 55, "Value still in storage.");
        subject.unset("test_storage");
        
        subject.setStoragePrefix("");
        assert.notEqual(subject.get("test_storage", true), 55, "Value not set in new prefix.");
        
        // Clean up
        subject.setStoragePrefix("testing");
        subject.unset("test_storage", true);
        assert.notEqual(subject.get("test_storage", true), 55, "Value was correctly removed from storage.");
        subject.setStoragePrefix("");
    });
    
    QUnit.test("GameData bucket.", function(assert){
        // Test bucket
        assert.notEqual(subject.get("test1"), 1, "Value no yet set.");
        assert.notEqual(subject.get("test2"), 2, "Value no yet set.");
        
        subject.set("test1", 1);
        subject.set("test2", 2);
        
        assert.equal(subject.get("test1"), 1, "Value was set correctly.");
        assert.equal(subject.get("test2"), 2, "Value was set correctly.");
        
        var bucket = subject.getBucket("bucket");
        assert.equal(Object.keys(bucket).length, 0, "Bucket empty.");
        
        subject.addToBucket("test1", "bucket");
        subject.addToBucket("test2", "bucket");
        
        bucket = subject.getBucket("bucket");
        assert.equal(Object.keys(bucket).length, 2, "Bucket contains 2 elements.");
        assert.equal(bucket.test1, 1, "Bucket has correct value.");
        assert.equal(bucket.test2, 2, "Bucket has correct value.");
        
        bucket = subject.getBucket("bucket_test");
        assert.equal(Object.keys(bucket).length, 0, "Bucket empty.");
        
        subject.removeFromBucket("test1", "bucket");
        bucket = subject.getBucket("bucket");
        assert.equal(Object.keys(bucket).length, 1, "Bucket contains 1 element.");
        assert.ok(!("test1" in bucket), "Value removed from bucket.");
        assert.equal(bucket.test2, 2, "Bucket has correct value.");
    });
    
    QUnit.test("GameData state.", function(assert){
        // Test bucket
        assert.notEqual(subject.get("test1"), 1, "Value no yet set.");
        assert.notEqual(subject.get("test2"), 2, "Value no yet set.");
        
        subject.set("test1", 1);
        subject.set("test2", 2);
        
        assert.equal(subject.get("test1"), 1, "Value was set correctly.");
        assert.equal(subject.get("test2"), 2, "Value was set correctly.");
        
        subject.saveStateAs("test_state");

        subject.unset("test1");
        assert.ok(subject.get("test1") == undefined, "Value was unset.");
        subject.set("test2", 3);
        assert.equal(subject.get("test2"), 3, "Value was changed.");
        
        subject.loadState("test_state");
        assert.equal(subject.get("test1"), 1, "Value was set reloaded.");
        assert.equal(subject.get("test2"), 2, "Value was set reloaded.");
        
        Arstider.savedStates = {};
    });
    
    QUnit.test("Class singleton integrity.", function(assert){
        var done = assert.async();
        subject.__testVal = "test";
        require(["Arstider/GameData"], function(s){
            assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
            done();
            //Cleanup
            delete subject.__testVal;
        });
    });
});