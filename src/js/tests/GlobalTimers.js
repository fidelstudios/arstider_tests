define("tests/GlobalTimers", ["module", "Arstider/GlobalTimers"], function (module, subject) {    
    
    var elem = {
        running: true,
        delay: 60,
        step: function(){
            if (this.delay <= 0)
            {
                this.running = false;
            }
        },
        pause: function(){
            this.running = false;
        },
        resume: function(){
            this.running = true;
        }
    };
        
    /**
     * Setup
     */
    QUnit.module(module.id, {
        afterEach: function() {
            // Clean up after test, to test in clean environment
            subject.list = [];
            elem.delay = 60;
            elem.running = true;
        }
    });

    /**
     * Tests
     */
    QUnit.test("GlobalTimers step.", function(assert){
        assert.equal(subject.count(), 0, "List empty.");
        
        subject.push(elem);
        
        assert.equal(subject.count(), 1, "Element added.");
        
        subject.step(33);
        
        assert.ok(elem.running, "Still running.");
        
        subject.step(33);
        
        assert.ok(!elem.running, "Finished running.");
        
        subject.remove(elem);
        
        assert.equal(subject.count(), 0, "Element removed.");
    });
    
    QUnit.test("GlobalTimers functions.", function(assert){
        assert.equal(subject.count(), 0, "List empty.");
        
        subject.push(elem);
        
        assert.equal(subject.count(), 1, "Element added.");
        assert.ok(elem.running, "Running.");
        subject.pauseAll();
        assert.ok(!elem.running, "Paused.");
        subject.resumeAll();
        assert.ok(elem.running, "Resumed.");
        
        subject.clean();
        
        assert.equal(subject.count(), 0, "Element removed.");
    });
    
    QUnit.test("Class singleton integrity.", function(assert){
        var done = assert.async();
        subject.__testVal = "test";
        require(["Arstider/GlobalTimers"], function(s){
            assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
            done();
            //Cleanup
            delete subject.__testVal;
        });
    });
});