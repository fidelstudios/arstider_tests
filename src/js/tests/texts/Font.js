define("tests/texts/Font", ["module", "Arstider/texts/Font"], function (module, subject) {    
    
    /**
     * Setup
     */
    QUnit.module(module.id, {
        afterEach: function() {
            var loader = window.document.getElementById("Arstider_font_loader");
            if (loader)
            {
                loader.parentNode.removeChild(loader);
            }
        }
    });

    /**
     * Tests
     */
    QUnit.test("Font loading.", function(assert){
        var done = assert.async();
        
        var e = new subject({
            "name": "titleFont",
            "url": "media/fonts/Dosis-Bold.woff",
            "padding": 5,
            "paddingTop": 10,
            "paddingLeft": 20,
            "lineSpacing": 30,
            "lineHeight": 33,
            "textWrap": true,
            "size": "28px",
            "bold": true,
            "italic": true,
            "fontOffsetY": 28,
            "fillStyle": "#000",
            "textAlign": "center",
            "loadCallbacks":[
            function(){
                assert.ok(e.loaded, "Font loaded.");
                assert.equal(e.padding, 5, "Font padding.");
                assert.equal(e.paddingTop, 10, "Font padding top.");
                assert.equal(e.paddingLeft, 20, "Font padding left.");
                assert.equal(e.lineSpacing, 30, "Font line spacing.");
                assert.equal(e.lineHeight, 33, "Font line height.");
                assert.ok(e.textWrap, "Font text wrap.");
                assert.ok(e.bold, "Font text bold.");
                assert.ok(e.italic, "Font text italic.");
                assert.equal(e.name, "titleFont", "Font name.");
                assert.equal(e.size, "28px", "Font size set.");
                assert.equal(e.fontOffsetY, 28, "Font y offset.");
                assert.equal(e.fillStyle, "#000", "Font fill style.");
                assert.equal(e.textAlign, "center", "Font text align.");
                
                done();}
            ]
        });
    });
});