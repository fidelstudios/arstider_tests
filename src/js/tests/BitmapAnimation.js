define("tests/BitmapAnimation", ["module", "Arstider/BitmapAnimation"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);

	/**
	 * Tests
	 */
	QUnit.test("Successful load anim", function(assert){
		var done = assert.async();
		var e = new subject({
			spritesheet:"media/images/akira",
			onComplete:function(){
				//Successful load
				assert.ok(true, "onComplete function triggered.");
	
				//Animation information
				assert.ok(e.spritesheet != null, "Spritesheet loaded");
				assert.ok(e.bitmaps["media/images/akira/akira.png"] != null, "Image loaded");
				
				//Animation play
				e.gotoAndPlay(5);
				assert.ok(e.currentFrame == 5 && e.isPlaying, "Go to frame 5 and play");

				e.gotoAndStop(3);
				assert.ok(e.currentFrame == 3 && !e.isPlaying, "Go to frame 3 and stop");
				
				e.gotoAndPlay("idle");
				assert.ok(e.animName == "idle" && e.currentFrame == 1 && e.isPlaying, "Go to anim and play");
				
				e.onunload = function(){
					assert.ok(e.spritesheet == null, "Spritesheet unloaded");
					require(["Arstider/sprites/SpritesheetManager"], function(s){
						// Reset SpritesheetManager cache for future test
						s.reset();
						done();
					});
				};
				e.killBuffer();
			}
		});
	});
});