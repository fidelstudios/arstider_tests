define("tests/Buffer", ["module", "Arstider/Buffer"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);

	/**
	 * Methods
	 */
	function drawTest(e){
		try{
			var testCanvas = document.createElement("canvas");
			var testCtx = testCanvas.getContext("2d");
			testCtx.drawImage(e, 0, 0);
			return true;
		}
		catch(rr){
			console.error(rr);
			return false;
		}
	}


	/**
	 * Tests
	 */
	QUnit.test("Save image to buffer", function(assert){
		var done = assert.async();
		var testURL = "media/images/test.png";
		//Save to buffer test
		var imgTest = new Image();
		imgTest.onload = function(){
			var test = Arstider.saveToBuffer("_imgTest", imgTest);
			assert.ok(test instanceof subject, "Save to buffer produces a buffer");
			assert.ok(Arstider.bufferPool["_imgTest"] == test, "Buffer stored in pool.");
			assert.ok(test.width == 538 && test.data.width == 538 && test.height == 190 && test.data.height == 190, "Buffer sizes are accurate.");
			assert.ok(drawTest(test.data), "Can draw buffer data.");
			assert.ok(test.getContext().drawImage, "Can refetch context.");

			//Test pixel methods
			var aPoint = test.getPixelAt(0, 0);
			aPoint.a = 1;
			aPoint.r = 128;
			var bPoint = test.setPixelAt(200, 54, aPoint);
			assert.ok(bPoint != null, "Change pixel data.");

			var oPoint = test.getPixelAt(200, 54);
			assert.ok(oPoint.a == 1 && oPoint.r == 128, "Pixel data changed permanently.");

			assert.ok(test.getAlphaAt(0,0) == 0, "Get get alpha value at a given position.");
			assert.ok(test.getAlphaAt(-5, -5) == null, "Get negative out of bound pixel handling.");
			assert.ok(test.getAlphaAt(5000, 5000) == null, "Get positive out of bound pixel handling.");

			//out of range pixel
			assert.ok(test.getPixelAt(0,0).a == 0, "Get get alpha value at a given position.");
			assert.ok(test.getPixelAt(-5, -5) == null, "Get negative out of bound pixel handling.");
			assert.ok(test.getPixelAt(5000, 5000) == null, "Get positive out of bound pixel handling.");

			test.kill();
			assert.ok(test.data == null && !Arstider.bufferPool["_imgTest"], "Can destroy buffers and their references.");

			done();
		};
		imgTest.src = testURL;		
	});

	QUnit.test("Create a new buffer", function(assert){
		var done = assert.async();
		var test = new subject({
			name:"_createdBuffer",
			width:800,
			height:900,
		});

		assert.ok(test instanceof subject && test.data && test.data instanceof HTMLCanvasElement, "Created a buffer with canvas element data.");
		assert.ok(Arstider.bufferPool["_createdBuffer"] == test, "Buffer stored in pool.");
		assert.ok(test.width == 800 && test.data.width == 800 && test.height == 900 && test.data.height == 900, "Buffer sizes are accurate.");
		assert.ok(drawTest(test.data), "Can draw buffer data.");
		assert.ok(test.getContext().drawImage, "Can refetch context.");

		//Test buffer modifications
		test.setSize(null, 300);
		assert.ok(test.width == 800 && test.data.width == 800 && test.height == 300 && test.data.height == 300, "Sizes changes are accurate.");

		assert.ok(test.updateRenderStyle("sharp"), "Can chnage render style.");
		assert.ok(!test.updateRenderStyle("unknown"), "Bad render style handling");

		var url = test.getURL(null, 0);
		assert.ok(typeof url === "string" && url.indexOf("data:") != -1, "Can get data url from buffer.");
		
		var img = new Image();
		img.onload = function(){
			assert.ok(drawTest(img), "Can draw data url of buffer.");

			test.kill();

			done();
		};
		img.onerror = function(){
			assert.ok(false, "Failed to load buffer-generated data url.");
			done();
		};
		img.src = url;
	});
});	