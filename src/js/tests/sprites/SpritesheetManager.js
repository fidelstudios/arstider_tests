define("tests/sprites/SpritesheetManager", ["module", "Arstider/sprites/SpritesheetManager"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);

	/**
	 * Tests
	 */
	QUnit.test("Load texture packer anim", function(assert){
		var done = assert.async();
		var path = "media/images/akira";
		var e = subject.get(
			path,
			{},
			function(spritesheet){
				//Successful load
				assert.ok(spritesheet != null, "Spritesheet loaded.");
				
				var exist = false;
				for (var key in subject.spritesheets) {
					if (subject.spritesheets[key] == spritesheet){
						exist = true;
						break;
					}
				}
				assert.ok(exist, "Spritesheet cached");
				
				subject.reset();
				assert.ok(Object.keys(subject.spritesheets).length == 0, "Spritesheet uncached");
				
				done();
			}
		);
	});
	
	QUnit.test("Load grid format anim", function(assert){
		var done = assert.async();
		var path = "media/images/jake";
		var e = subject.get(
			path,
			{},
			function(spritesheet){
				//Successful load
				assert.ok(spritesheet != null, "Spritesheet loaded.");
				
				var exist = false;
				for (var key in subject.spritesheets) {
					if (subject.spritesheets[key] == spritesheet){
						exist = true;
						break;
					}
				}
				assert.ok(exist, "Spritesheet cached");
				
				subject.reset();
				assert.ok(Object.keys(subject.spritesheets).length == 0, "Spritesheet uncached");
				
				done();
			}
		);
	});
	
	QUnit.test("Class singleton integrity.", function(assert){
		var done = assert.async();
		subject.__testVal = "test";
		require(["Arstider/sprites/SpritesheetManager"], function(s){
			assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
			done();
			//Cleanup
			delete subject.__testVal;
		});
	});
});