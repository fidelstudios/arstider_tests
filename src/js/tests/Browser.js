define("tests/Browser", ["module", "Arstider/Browser"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);


	/**
	 * Tests
	 */
	QUnit.test("Class singleton integrity.", function(assert){
		var done = assert.async();
		subject.__testVal = "test";
		require(["Arstider/Browser"], function(s){
			assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
			done();
			//Cleanup
			delete subject.__testVal;
		});
	});
});		