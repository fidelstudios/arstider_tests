define("tests/Events", ["module", "Arstider/Events"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);

	/**
	 * Tests
	 */
	QUnit.test("Bind.", function(assert){
		// Track function is called once
		var once = true;
		
		subject.bind("Event.test", function(){
			assert.ok(once, "Event triggered");
			
			// Set that function was called once
			once = false;
		});
		
		subject.bind("Event.test2", function(){
			assert.ok(false, "Should not triggered");
		});
		
		// Call event
		subject.broadcast("Event.test");
		
		// Remove all event
		subject.flush();
		
		// Try to call removed event
		subject.broadcast("Event.text");
	});
	
	QUnit.test("Unbind.", function(assert){
		// Number of assertions expected
		assert.expect (1);
		
		// Track function is called once
		var once = true;
		
		subject.bind("Event.test", function(){
			assert.ok(once, "Event triggered once");
			
			// Set that function was called once
			once = false;
		});
		
		// Try to remove event with wrong function
		subject.unbind("Event.test", function(){});
		
		// Call event
		subject.broadcast("Event.test");
		
		// Remove all event Event.test
		subject.unbind("Event.test");
		
		// Try to call removed event
		subject.broadcast("Event.text");
	});
	
	QUnit.test("Flush.", function(assert){
		// Number of assertions expected
		assert.expect (1);
		
		// Track function is called once
		var once = true;
		
		subject.bind("Event.test", function(){
			assert.ok(once, "Event triggered once");
			
			// Set that function was called once
			once = false;
		});
		
		// Call event
		subject.broadcast("Event.test");
		
		// Remove all event
		subject.flush();
		
		// Try to call removed event
		subject.broadcast("Event.text");
	});
	
	QUnit.test("Class singleton integrity.", function(assert){
		var done = assert.async();
		subject.__testVal = "test";
		require(["Arstider/Events"], function(s){
			assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
			done();
			//Cleanup
			delete subject.__testVal;
		});
	});
});		