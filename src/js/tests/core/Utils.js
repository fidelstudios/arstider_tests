define("tests/core/Utils", ["module"], function (module) {    
    
    /**
     * Setup
     */
    QUnit.module(module.id);

    /**
     * Tests
     */
    QUnit.test("Arstider math.", function(assert){
        // Min
        assert.equal(Arstider.min(10, 100), 10, "Minimum positive.");
        assert.equal(Arstider.min(-10, -100), -100, "Minimum negative.");
        
        // Max
        assert.equal(Arstider.max(99, 9), 99, "Maximum positive.");
        assert.equal(Arstider.max(-99, -9), -9, "Maximum negative.");
        
        // Floor
        assert.equal(Arstider.floor(10.7898), 10, "Floor positive.");
        
        // Ceil
        assert.equal(Arstider.ceil(98.2134), 99, "Ceil positive.");
        
        // Power of 2
        assert.ok(Arstider.powerOf2(16), "Number is power of 2.");
        assert.ok(!Arstider.powerOf2(12), "Number is not power of 2.");
        
        // Next power of 2
        assert.equal(Arstider.nextPowerOf2(43000), 65536, "Next power of 2.");
        assert.equal(Arstider.nextPowerOf2(128), 128, "Next power of 2 of power of 2.");
        
        // Distance 2d
        assert.equal(Arstider.distance(0, 0, 3, 4), 5, "Distance in 2d.");
        
        // Distance 3d
        assert.equal(Arstider.distance3(0, 0, 0, 4, 4, 7), 9, "Distance in 3d.");
    });
});