define("tests/core/Storage", ["module", "Arstider/core/Storage"], function (module, subject) {    
    
    /**
     * Setup
     */
    QUnit.module(module.id, {
        afterEach: function() {
            // Clean up after test, to test in clean environment
            subject._safeKeys = [];
            subject.reset();
            subject.prefix = "";
        }
    });

    /**
     * Tests
     */
    QUnit.test("Class singleton integrity.", function(assert){
        // Test storage
        assert.ok(subject.get("test_storate") == null, "Key not set.");
        
        subject.set("test_storage", 2);
        assert.equal(subject.get("test_storage"), 2, "Value was set correctly.");
        
        subject.remove("test_storage");
        assert.ok(subject.get("test_storate") == null, "Key was removed.");
        
        subject.prefix = "this_test";
        var list = subject.list();
        assert.equal(list.length, 0, "List empty.");
        subject.setSafeKeys(["test_storage2"]);
        
        subject.set("test_storage1", 1);
        subject.set("test_storage2", 2);
        
        list = subject.list();
        assert.ok(list.length, 2, "List contains 2 items.");
        
        subject.reset();
        
        assert.equal(subject.get("test_storage1"), null, "Key was removed");
        assert.equal(subject.get("test_storage2"), 2, "Key was not removed");
    });
    
    QUnit.test("Class singleton integrity.", function(assert){
        var done = assert.async();
        subject.__testVal = "test";
        require(["Arstider/core/Storage"], function(s){
            assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
            done();
            //Cleanup
            delete subject.__testVal;
        });
    });
});