define("tests/Background", ["module", "Arstider/Background"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);


	/**
	 * Tests
	 */
	QUnit.test("Class inherits DisplayObject", function(assert){
		var done = assert.async();
		require(["Arstider/DisplayObject"], function(s){
			assert.ok(subject instanceof s, "Background does inherit DisplayObject");

			assert.ok(typeof subject.loadBitmap === "function", "Can call loadBitmap on Background.");
			done();
		});
	});
	QUnit.test("Class singleton integrity.", function(assert){
		var done = assert.async();
		subject.__testVal = "test";
		require(["Arstider/Background"], function(s){
			assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
			done();
			//Cleanup
			delete subject.__testVal;
		});
	});
});		