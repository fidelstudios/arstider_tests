define("tests/Dictionary", ["module", "Arstider/Dictionary"], function (module, subject) {	
	
	/**
	 * Setup
	 */
	QUnit.module(module.id);

	/**
	 * Tests
	 */
	QUnit.test("File load.", function(assert){
		var done = assert.async();
		subject.load(
			"media/strings.json",
			function(){
				assert.ok(true, "File loaded complete.");
				assert.equal(subject.translate("TEST1"), "success", "Translation done.");
				assert.equal(subject.translate("TEST2", {"{{test}}":20}), "replace 20", "Translation and replace done.");
				done();
			}
		);
	});
	
	QUnit.test("Class singleton integrity.", function(assert){
		var done = assert.async();
		subject.__testVal = "test";
		require(["Arstider/Dictionary"], function(s){
			assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
			done();
			//Cleanup
			delete subject.__testVal;
		});
	});
});