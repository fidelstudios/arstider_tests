define("tests/Mouse", ["module", "Arstider/Mouse", "Arstider/Viewport"], function (module, subject, Viewport) {    
        
    /**
     * Setup
     */
    QUnit.module(module.id, {
        beforeEach: function() {
            subject.init(document);
        },
        afterEach: function() {
            subject.removeListener(document);
        }
    });

    /**
     * Tests
     */
    QUnit.test("Mouse event.", function(assert){
        assert.ok(!subject.isPressed(), "Left click not pressed.");
        var event = new MouseEvent("mousedown", {
            clientX: 10,
            clientY: 15,
            button: 0
        });
        document.dispatchEvent(event);
        assert.ok(subject.isPressed() && !subject.isPressed("right"), "Left click pressed.");
        
        assert.ok(subject.x() == 10 && subject.y() == 15, "Get mouse position.");
        var mouseMoveEvent = new MouseEvent("mousemove", {
            clientX: 130,
            clientY: 215,
            button: 0
        });
        document.dispatchEvent(mouseMoveEvent);
        assert.ok(subject.x() == 130 && subject.y() == 215, "Get new mouse position.");
        
        var mouseDownEvent = new MouseEvent("mousedown", {
            clientX: 100,
            clientY: 45,
            button: 2
        });
        document.dispatchEvent(mouseDownEvent);
        assert.ok(subject.isPressed("right"), "Right click pressed.");
        assert.ok(subject.x() == 100 && subject.y() == 45, "Get new mouse position.");
        
        subject.reset();
        assert.ok(!subject.isPressed() && !subject.isPressed("right") && subject.x() == 0 && subject.y() == 0, "Reset info.");
    });
    
    QUnit.test("Class singleton integrity.", function(assert){
        var done = assert.async();
        subject.__testVal = "test";
        require(["Arstider/Mouse"], function(s){
            assert.equal(subject.__testVal, s.__testVal, "Singleton instance not re-instantiated.");
            done();
            //Cleanup
            delete subject.__testVal;
        });
    });
});