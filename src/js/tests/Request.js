define("tests/Request", ["module", "Arstider/Request"], function (module, subject) {    
    
    /**
     * Setup
     */
    QUnit.module(module.id, {
        afterEach: function() {
            subject.flushCache();
        }
    });

    /**
     * Tests
     */
    QUnit.test("Request image.", function(assert){
        var done = assert.async(), progress = 0, thisRef = this;
        
        var r = new subject({
            url: "media/images/test.png",
            caller: this,
            callback: function(data) {
                assert.ok(data instanceof Blob, "Callback with right data type.");
                assert.ok(data.type == "image/png", "Data type ok.");
                assert.ok(data.size == 14022, "Size ok.");
                assert.ok(progress == 1, "Progress changed.");
                assert.ok(this == thisRef, "Resquest caller ok.");
                done();
            },
            progress: function(e) {
                progress = e.loaded/e.total;
            }
        });
        r.send();
    });
    
    QUnit.test("Request json in text.", function(assert){
        var done = assert.async();
        
        var r = new subject({
            url: "media/strings.json",
            mimeOverride: "application/json",
            callback: function(data) {
                assert.ok(data.type == "application/json", "Correct type override.");
                var fileReader = new FileReader();
                fileReader.onload = function() {
                    var json = JSON.parse(fileReader.result);
                    assert.equal(json.TEST1, "success", "Json content ok.");
                    done();
                };
                
                fileReader.readAsText (data);
            }
        });
        r.send();
    });
    
    QUnit.test("Request cache.", function(assert){
        var done1 = assert.async(),
            done2 = assert.async(),
            cache1 = false,
            cache2 = true;
        
        var r = new subject({
            url: "media/images/test.png",
            cache: true,
            callback: function() {
                assert.ok(cache1, "Image was not cached.");
                done1();
                
                var r = new subject({
                    url: "media/images/test.png",
                    cache: true,
                    callback: function() {
                        assert.ok(cache2, "Image was cached.");
                        done2();
                    }
                }).send();
                
                // Must be set after callback is called
                cache2 = false;
            }
        }).send();
                
        // Must be set before callback is called
        cache1 = true;
    });
});